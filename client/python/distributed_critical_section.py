from kazoo.client import KazooClient
from kazoo.recipe.lock import Lock
import random
import time
import logging
import os

# Konfigurace Apache ZooKeeper
ZOOKEEPER_HOSTS = os.getenv('ZOO_SERVERS')
ZOOKEEPER_APP_PATH = '/application'
ZOOKEEPER_STATE_PATH = ZOOKEEPER_APP_PATH + '/state'
ZOOKEEPER_SECTION_PATH = ZOOKEEPER_APP_PATH + '/section'

# Konfigurace aplikace
MIN_WAIT_TIME = 5  # Minimalni cas straveny v kriticke sekci (v sekundach)
MAX_WAIT_TIME = 15  # Maximalni cas straveny v kriticke sekci (v sekundach)


# Trida reprezentujici distribuovanou aplikaci
class DistributedApp:
    def __init__(self):
        self.zk = KazooClient(hosts=ZOOKEEPER_HOSTS)
        self.lock = Lock(self.zk, ZOOKEEPER_SECTION_PATH)
        self.node_id = None

    def connect(self):
        self.zk.start()

    def disconnect(self):
        self.zk.stop()


    def enter_critical_section(self):
        with self.lock:
            wait_time = random.randint(MIN_WAIT_TIME, MAX_WAIT_TIME)
            print(f"{self.node_id} vstoupil do kriticke sekce a stravi zde {wait_time} sekund.")
            time.sleep(wait_time)
            print(f"{self.node_id} opustil kritickou sekci.")
            self.lock.release

    def monitor_node_state(self):
        @self.zk.DataWatch(ZOOKEEPER_STATE_PATH)
        def watch_node_state(data, stat):
            if data:
                print(f"Stav {self.node_id}: {data.decode()}")

    def run(self):
        self.connect()

        # Vytvoreni struktury
        if not self.zk.exists(ZOOKEEPER_APP_PATH):
            self.zk.create(ZOOKEEPER_APP_PATH)
        if not self.zk.exists(ZOOKEEPER_STATE_PATH):
            self.zk.create(ZOOKEEPER_STATE_PATH)
        if not self.zk.exists(ZOOKEEPER_SECTION_PATH):
            self.zk.create(ZOOKEEPER_SECTION_PATH)

        self.node_id = os.uname()[1]
   
        while True:
            print(f"{self.node_id} ceka na vstup do kriticke sekce!")

            # Pokus o vstup do kriticke sekce
            self.enter_critical_section()

            time.sleep(1)

        self.disconnect()


# Spuštění aplikace
if __name__ == '__main__':
    app = DistributedApp()
    app.run()
