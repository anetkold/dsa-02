
**Zadání:**
	Zadáním druhé semestrální práce na předmět KIV/DSA bylo implementovat distribuovanou kritickou sekci.
	
**Implementace:** 
K implementaci bylo využito nástrojů Vagrant a Docker. Aplikace byla programována v jazyce Python.
Jedná se o jeden skript, který po spuštění chce vstoupit do kritické sekce. Pokud je kritická sekce volná, pak do ní vstoupí a zůstane tam náhodnou dobu, která se pohybuje mezi pěti až patnácti sekundami. 
Jestliže kritická sekce volná není, pak proces vypíše pouze úvodní hlášku o svém požadavku na vstup a počká, dokud proces, který v kritické sekci je, neukončí svojí činnost a z kritické sekce nejvyde. Následně do ní vstoupí další proces v pořadí.

**Sestavení:**
	K sestavení aplikací slouží příkaz `vagrant up'` který z přítomného souboru ‘**Dockerfile**’ vytvoří strukturu, kterou následně také spustí. Pro sledování průběhu volby lze použít příkaz `watch docker logs client-X,` kde X je číslo sledovaného uzlu. 
	Aplikace na výstup vypisuje informace o jednotlivých krocích.
